package com.mountblue;

import com.mountblue.Interface.Service;
import com.mountblue.Interface.Sim;

import java.security.PrivateKey;

public class Airtel implements Sim {
    private Service service;

    public void setService(Service service) {
        this.service = service;
    }
    public void activateService(){
        service.service();
    }

    @Override
    public void calling() {
        System.out.println("Calling from Airtel Sim");
    }
}
