package com.mountblue;

import com.mountblue.Interface.Sim;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Mobile {
    public static void main(String[] args) {
        ApplicationContext context= new ClassPathXmlApplicationContext("bean.xml");
        Sim sim= context.getBean("airtel",Sim.class);
        Airtel airtel=context.getBean("airtel",Airtel.class);
        sim.calling();
        airtel.activateService();
    }
}
